6/26 - today we're working on back-end authentication and learning how to best use git as a team. Jkwon is driving for our pair-programming team development.
6/27 - have working signup and sign in/sign out. to do: creating protected routes, accessing token, frontend auth
6/28 - nariman driving today. we're working on refactoring our auth, handling duplicate accounts, adding env file and separating concerns, adding token route
6/29 - xiaodi driving today. today we're working on refactoring our trip model to include account data and checking account data for delete permissions
6/30 - multiple drivers, set up signup and trip list components.
7/10 - today we worked on our protected user routes for listing, deleting, and updating trips for authorized users. jkwon driving.
7/11 - refactorred to rtk query using redux library and refactoring auth. jkwon driving
7/12 - xiaodi driving - plan is to add error handling to login and signup forms, add rtq query to some of our api routes
7/13 - nariman driving/multiple drivers - refactor error handling for auth, exploring styling our components
7/14 - jkwon driving adding rtk query to trips routes, multiple drivers implementing unit tests for routes
7/17 - multiple drivers - rolled up tests and worked on fixed linting errors. tomorrow working on trip form, will need to adjust model routes, queries.
