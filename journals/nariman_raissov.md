Week 1.
    For this project, we decided to use MongoDB.
    We focused on backend for accounts, created routers with endpoint and queries, implemented authentication for users.
Week 2.
    Created backend endpoints routers and queires for trips. Wrote unit tests for trips. Started working on front end.
Week 3.
    We decided to use Redux. Created UI pages.
Week 4.
    Added more CSS. Added Google Maps API.