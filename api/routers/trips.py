from fastapi import (
    APIRouter,
    Depends,
)
from models import TripList, TripIn, TripOut
from authenticator import authenticator
from queries.trips import TripsQueries


router = APIRouter()


@router.get("/api/trips", response_model=TripList)
def list_trips(repo: TripsQueries = Depends()):
    return TripList(trips=repo.get_all_trips())


@router.post("/api/trips", response_model=TripOut)
async def create_trip(
    trip_in: TripIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: TripsQueries = Depends(),
):
    trip = queries.create(trip_in, account=account_data)
    print(trip)
    print(account_data)
    return TripOut(**trip.dict())


@router.put("/api/trips/{id}")
def update(
    id: str,
    trip_out: TripOut,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: TripsQueries = Depends(),
):
    return queries.update(id=id, trip=trip_out, account=account_data)


@router.get("/api/trips/view/{id}")
def view_trip(
    id: str,
    queries: TripsQueries = Depends(),
):
    return queries.view_trip(id=id)
