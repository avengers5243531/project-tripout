from fastapi import (
    APIRouter,
    Depends,
)
from models import TripList
from authenticator import authenticator
from queries.trips import TripsQueries


router = APIRouter()


@router.get("/api/trips/mine", response_model=TripList)
def my_list_trips(
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: TripsQueries = Depends(),
):
    return {"trips": queries.get_my_trips(account_id=account_data["id"])}


@router.delete("/api/trips/mine/{id}")
def delete_my_trip(
    id: str,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: TripsQueries = Depends(),
):
    return {"trips": queries.delete(id=id, account_id=account_data["id"])}
