from models import TripIn, TripOut, AccountOut
from queries.client import MongoQueries
from bson.objectid import ObjectId


class TripsQueries(MongoQueries):
    collection_name = "trips"

    def create(self, trip_in: TripIn, account: AccountOut):
        trip = trip_in.dict()
        trip["account"] = account
        trip["attendees"] = []
        self.collection.insert_one(trip)
        trip["id"] = str(trip["_id"])
        return TripOut(**trip)

    def get_all_trips(self):
        result = []
        for trip in self.collection.find():
            trip["id"] = str(trip["_id"])
            result.append(trip)
        return result

    def delete(self, id: str, account_id):
        result = self.collection.find_one({"_id": ObjectId(id)})
        if account_id == result["account"]["id"]:
            result_deleted = self.collection.delete_one({"_id": ObjectId(id)})
            print(result_deleted)
            return result_deleted.deleted_count > 0
        else:
            return {"message": "not authorized"}

    def update(self, id: str, trip: TripOut, account: AccountOut):
        trip = trip.dict()
        self.collection.update_one({"_id": ObjectId(id)}, {"$set": trip})
        return TripOut(**trip)

    def get_my_trips(self, account_id: str):
        results = []
        for trip in self.collection.find({"account.id": account_id}):
            trip["id"] = str(trip["_id"])
            results.append(trip)
            print(results)
        return results

    def view_trip(self, id: str):
        trip = self.collection.find_one({"_id": ObjectId(id)})
        trip["id"] = str(trip.pop("_id"))
        return trip
