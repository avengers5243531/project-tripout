from queries.client import MongoQueries
from models import (AccountIn,
                    AccountOutWithHashedPassowrd,
                    DuplicateAccountError)


class AccountQueries(MongoQueries):

    collection_name = 'user-accounts'

    def get(self, username: str):
        account = self.collection.find_one({"username": username})
        if account is None:
            return None
        account['id'] = str(account['_id'])
        return AccountOutWithHashedPassowrd(**account)

    def create(self, account_in: AccountIn, hashed_password: str):
        account_in = account_in.dict()
        if self.get(account_in['username']) is not None:
            raise DuplicateAccountError
        account_in['hashed_password'] = hashed_password
        del account_in['password']
        self.collection.insert_one(account_in)
        account_in['id'] = str(account_in['_id'])
        return AccountOutWithHashedPassowrd(**account_in)
