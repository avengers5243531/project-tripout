import os
from pymongo import MongoClient


DATABASE_URL = os.environ.get("DATABASE_URL")
client = MongoClient(DATABASE_URL)
db = client["trips-database"]


class MongoQueries:
    @property
    def collection(self):
        return db[self.collection_name]
