import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import "./index.css";
import "bootstrap/dist/css/bootstrap.css";
import "bootswatch/dist/lux/bootstrap.css";
import Home from "./Home";
import Trips from "./Trips";
import MyJoinedTrips from "./MyJoinedTrips.js";
import TripForm from "./CreateTrip";
import MyTrips from "./MyTrips";
import TripView from "./TripView";
import EditForm from "./EditForm";
import PrivacyLegal from "./PrivacyLegal";
import ContactInfo from "./ContactInfo";
import About from "./About";
import ErrorPage from "./ErrorPage";
import LoginForm from "./LoginForm";
import SignupForm from "./SignupForm";
import reportWebVitals from "./reportWebVitals";
import { store } from "./app/store";
import { Provider } from "react-redux";
import { createBrowserRouter, RouterProvider } from "react-router-dom";

const domain = /https:\/\/[^/]+/;
const basename = process.env.PUBLIC_URL.replace(domain, "");

const router = createBrowserRouter(
  [
    {
      element: <App />,
      errorElement: <ErrorPage />,
      children: [
        {
          path: "/",
          element: <Home />,
        },
        {
          path: "/login",
          element: <LoginForm />,
        },
        {
          path: "/signup",
          element: <SignupForm />,
        },
        {
          path: "/trips/create",
          element: <TripForm />,
        },
        {
          path: "/trips",
          element: <Trips />,
        },
        {
          path: "/trips/mine",
          element: <MyTrips />,
        },
        {
          path: "/trips/joined",
          element: <MyJoinedTrips />,
        },
        {
          path: `/trips/view/:id`,
          element: <TripView />,
        },
        {
          path: `/trips/:id`,
          element: <EditForm />,
        },
        {
          path: "/privacy",
          element: <PrivacyLegal />,
        },
        {
          path: "/contact-us",
          element: <ContactInfo />,
        },
        {
          path: "/about",
          element: <About />,
        },
      ],
    },
  ],
  { basename }
);

const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  <Provider store={store}>
    <RouterProvider router={router} />
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
