import { configureStore } from "@reduxjs/toolkit";
import { authApi } from "./authApiSlice.js";
import { setupListeners } from "@reduxjs/toolkit/query";
import { tripsApi } from "./tripsApiSlice.js";

export const store = configureStore({
  reducer: {
    [authApi.reducerPath]: authApi.reducer,
    [tripsApi.reducerPath]: tripsApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(authApi.middleware, tripsApi.middleware),
});

setupListeners(store.dispatch);
