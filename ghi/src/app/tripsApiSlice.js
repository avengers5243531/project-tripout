import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const tripsApi = createApi({
  reducerPath: "tripsApi",
  baseQuery: fetchBaseQuery({ baseUrl: process.env.REACT_APP_API_HOST }),
  endpoints: (builder) => ({
    getAllTrips: builder.query({
      query: () => "/api/trips",
      providesTags: ["Trips"],
    }),
    createTrip: builder.mutation({
      query: (body) => ({
        url: "/api/trips",
        method: "POST",
        body,
        credentials: "include",
      }),
      invalidatesTags: ["Trips"],
    }),
    update: builder.mutation({
      query: (trip) => ({
        url: `/api/trips/${trip.id}`,
        method: "PUT",
        body: trip,
        credentials: "include",
      }),
      invalidatesTags: ["Trips"],
    }),
    getMyTrips: builder.query({
      query: () => ({
        url: "api/trips/mine",
        credentials: "include",
      }),
      providesTags: ["MyTrips"],
    }),
    viewTrip: builder.query({
      query: (id) => ({
        url: `/api/trips/view/${id}`,
        credentials: "include",
      }),
      invalidatesTags: ["Trips"],
    }),
    delete: builder.mutation({
      query: (id) => ({
        url: `/api/trips/mine/${id}`,
        method: "DELETE",
        credentials: "include",
      }),
      invalidatesTags: ["MyTrips"],
    }),
  }),
});

export const {
  useGetAllTripsQuery,
  useCreateTripMutation,
  useGetMyTripsQuery,
  useViewTripQuery,
  useDeleteMutation,
  useUpdateMutation,
} = tripsApi;
