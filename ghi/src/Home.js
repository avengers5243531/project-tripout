import React from "react";
import Background from "./Background";
import WebsiteIcon from "./assets/Website-Icons.png";
import WebsiteIcon2 from "./assets/new-post.png";
import { NavLink } from "react-router-dom";
import Footer from "./Footer";

export default function Home() {
  return (
    <>
      <Background />
      <div className="container-fluid">
        <div className="landing-container container text-center">
          <div className="row home-button-container">
            <div className="col col-button">
              <button className="btn bg-transparent">
                <NavLink to={"/trips/create"} className={"nav-link"}>
                  <img
                    src={WebsiteIcon2}
                    className="website-icon"
                    alt="create trip icon"
                  />
                </NavLink>
              </button>
              <h3>Create A Trip</h3>
            </div>
            <div className="col col-button">
              <button className="btn bg-transparent">
                <NavLink to={"/trips"} className={"nav-link"}>
                  <img
                    src={WebsiteIcon}
                    className="website-icon"
                    alt="trips icon"
                  />
                </NavLink>
              </button>
              <h3>Trips</h3>
            </div>
          </div>
        </div>
        <div>
          <br />
          <Footer />
        </div>
      </div>
    </>
  );
}
