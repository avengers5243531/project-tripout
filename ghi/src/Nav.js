import { NavLink } from "react-router-dom";
import { useGetAccountQuery, useLogoutMutation } from "./app/authApiSlice";
const Nav = () => {
  const { data: account } = useGetAccountQuery();
  const [logout] = useLogoutMutation();
  return (
    <nav className="navbar navbar-expand-lg">
      <div className="container-fluid">
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            {account && (
              <li className="nav-item">
                <NavLink to={"/"} className={"nav-link"}>
                  HOME
                </NavLink>
              </li>
            )}
            {account && (
              <li className="nav-item">
                <NavLink to={"/trips/create"} className={"nav-link"}>
                  Create Trip
                </NavLink>
              </li>
            )}
            {!account && (
              <li className="nav-item">
                <NavLink to={"/login"} className={"nav-link"}>
                  Login
                </NavLink>
              </li>
            )}
            {!account && (
              <li className="nav-item">
                <NavLink to={"/signup"} className={"nav-link"}>
                  Sign Up
                </NavLink>
              </li>
            )}
            {account && (
              <li className="nav-item">
                <NavLink to={"/trips"} className={"nav-link"}>
                  Trips
                </NavLink>
              </li>
            )}
            {account && (
              <li className="nav-item">
                <NavLink to={"/trips/mine"} className={"nav-link"}>
                  My Trips
                </NavLink>
              </li>
            )}
            {account && (
              <li className="nav-item">
                <NavLink to={"/trips/joined"} className={"nav-link"}>
                  Joined Trips
                </NavLink>
              </li>
            )}
          </ul>
          {account && (
            <>
              <div className="navbar-text" style={{ marginRight: "10px" }}>
                Logged in as: {account.username}
              </div>

              <NavLink to={"/"}>
                <button className="btn btn-outline-danger" onClick={logout}>
                  Logout
                </button>
              </NavLink>
            </>
          )}
        </div>
      </div>
    </nav>
  );
};
export default Nav;
