import React from "react";
import Footer from "./Footer";

function About() {
  return (
    <div>
      <div>
        <h1>About Us</h1>
        <p>
          Welcome to our web app, where you can create and join trips! Our
          platform allows users to plan exciting trips and invite others to join
          them on their adventures.
        </p>
        <p>
          With our easy-to-use interface, you can create trip events, specify
          the destination, date, and other details. Once your trip is created,
          other users can discover and join your trip if they're interested.
        </p>
        <p>
          We aim to connect like-minded travelers and make trip planning a
          breeze. So, start creating your dream trips and exploring the world
          together!
        </p>
      </div>
      <div>
        <br />
        <br />
        <br />
        <Footer />
      </div>
    </div>
  );
}

export default About;
