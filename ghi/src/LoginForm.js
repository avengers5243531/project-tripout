import { useLoginMutation } from "./app/authApiSlice";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import AlertError from "./AlertError";

const LoginForm = () => {
  const navigate = useNavigate();
  const [login, loginResult] = useLoginMutation();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    if (loginResult.error) {
      setErrorMessage(loginResult.error.data.detail);
    }
    if (loginResult.isSuccess) navigate("/trips");
  }, [loginResult, navigate]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    login({ username, password });
  };

  return (
    <div className="d-flex justify-content-center">
      <div className="auth card text-bg-light mb-3">
        <h5 className="card-header">Login</h5>
        {errorMessage && <AlertError>{errorMessage}</AlertError>}
        <div className="card-body">
          <form onSubmit={(e) => handleSubmit(e)}>
            <div className="mb-3">
              <label className="form-label">Username:</label>
              <input
                name="username"
                type="text"
                className="form-control"
                onChange={(e) => setUsername(e.target.value)}
                required
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Password:</label>
              <input
                name="password"
                type="password"
                className="form-control"
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </div>
            <div>
              <input className="btn btn-primary" type="submit" value="Login" />
            </div>
          </form>
          <div className="signup-footer mb-3">
            <p>
              Don't have an account? <Link to="/signup">Sign Up</Link>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoginForm;
