import Footer from "./Footer";
import { NavLink } from "react-router-dom";
function ContactInfo() {
  return (
    <div>
      <div>
        <h1>Contact Us</h1>
        <p>
          We'd love to hear from you! If you have any questions, feedback, or
          inquiries, please don't hesitate to get in touch with us using the
          contact information provided below:
        </p>
        <div>
          <strong>Email:</strong> tripwithus@gmail.com
        </div>
        <div>
          <strong>Phone:</strong> +1 (123) 456-7890
        </div>
        <p>
          You can also follow us on social media for updates and exciting trip
          ideas:
        </p>
        <div class="nav-link">
          <strong>Facebook:</strong>{" "}
          <NavLink to="https://www.facebook.com/">
            facebook.com/tripwithus
          </NavLink>
        </div>
        <div class="nav-link">
          <strong>Twitter:</strong>{" "}
          <NavLink to="https://twitter.com/i/flow/login?redirect_after_login=%2F">
            twitter.com/tripwithus
          </NavLink>
        </div>
        <div class="nav-link">
          <strong>Instagram:</strong>{" "}
          <NavLink to="https://www.instagram.com/">
            instagram.com/tripwithus
          </NavLink>
        </div>
      </div>
      <div>
        <br />
        <br />
        <br />
        <Footer />
      </div>
    </div>
  );
}

export default ContactInfo;
