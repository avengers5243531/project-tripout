import Footer from "./Footer";

function PrivacyLegal() {
  return (
    <div>
      <div>
        <h1>Privacy Policy</h1>
        <p>
          We take your privacy seriously. This Privacy Policy outlines how we
          collect, use, and disclose your personal information when you use our
          web app. By using our service, you agree to the collection and use of
          information as described in this policy.
        </p>
      </div>
      <div>
        <h1>Legal Disclaimer</h1>
        <p>
          The information provided on this web app is for general informational
          purposes only. We do not claim or guarantee the accuracy,
          completeness, or reliability of any information presented.
        </p>
        <p>
          The use of this web app is at your own risk. We are not responsible
          for any damages or losses resulting from the use of the information
          provided here.
        </p>
        <p>
          Please consult legal or travel professionals for specific advice
          related to your situation. Additionally, the use of this web app is
          subject to our Terms of Service and Privacy Policy.
        </p>
        <div>
          <br />
          <br />
          <br />
          <Footer />
        </div>
      </div>
    </div>
  );
}

export default PrivacyLegal;
