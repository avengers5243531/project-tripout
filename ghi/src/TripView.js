import React, { useState, useEffect } from "react";
import { useViewTripQuery } from "./app/tripsApiSlice";
import { useParams } from "react-router";
import CalendarIcon from "./assets/calendar.png";
import AttendeesIcon from "./assets/people.png";
import TypeIcon from "./assets/categories.png";
import DescriptionIcon from "./assets/info.png";
import UserIcon from "./assets/email.png";
import Map from "./Map";
import Footer from "./Footer";

export default function TripView() {
  let { id } = useParams();
  const { data, isLoading } = useViewTripQuery(id);
  const [cityLatLng, setCityLatLng] = useState(null);

  const defaultCenter = { lat: 0, lng: 0 };
  const center = cityLatLng || defaultCenter;
  const zoom = 10;
  const markers = [{ position: center }];

  useEffect(() => {
    if (data) {
      fetchCityLatLng(data.location.city);
    }
  }, [data]);

  const fetchCityLatLng = async (city) => {
    const apiKey = process.env.REACT_APP_GOOGLE_API_KEY;
    const geocodingApiUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${city}&key=${apiKey}`;

    try {
      const response = await fetch(geocodingApiUrl);
      const data = await response.json();

      if (data.status === "OK" && data.results.length > 0) {
        const { lat, lng } = data.results[0].geometry.location;
        setCityLatLng({ lat, lng });
      } else {
        console.error("Error fetching location");
      }
    } catch (error) {
      console.error("Error fetching location:", error);
    }
  };

  if (isLoading) {
    return <div>Loading...</div>;
  }

  return (
    <div className="trip-detail">
      <h2>{data.name}</h2>
      <h6>Created by {data.account.username} </h6>
      <img src={data.image} alt={data.name} />
      <div className="trip-info">
        <div className="trip-form">
          <img
            src={CalendarIcon}
            alt="Calendar-icon"
            style={{
              verticalAlign: "middle",
              marginRight: "10px",
              width: "25px",
              height: "25px",
              marginTop: "15px",
            }}
          />
          <strong>From: </strong>
          {data.from_date}
        </div>
        <div
          className="trip-form"
          style={{ marginLeft: "37px", marginBottom: "35px" }}
        >
          <strong>To:</strong> {data.to_date}
        </div>
        <div className="trip-form">
          <img
            src={AttendeesIcon}
            alt="Attendees-icon"
            style={{
              verticalAlign: "middle",
              marginRight: "10px",
              width: "28px",
              height: "28px",
              marginTop: "15px",
            }}
          />
          <strong>Max Attendees:</strong> {data.max_attendees}
        </div>
        <div className="trip-form">
          <img
            src={TypeIcon}
            alt="Type-icon"
            style={{
              verticalAlign: "middle",
              marginRight: "10px",
              width: "28px",
              height: "28px",
              marginTop: "15px",
            }}
          />
          <strong>Type:</strong> {data.type_of_trip}
        </div>
        <div className="trip-form">
          <img
            src={DescriptionIcon}
            alt="Descrition-icon"
            style={{
              verticalAlign: "middle",
              marginRight: "10px",
              width: "29px",
              height: "29px",
              marginTop: "15px",
            }}
          />
          <strong>Description: </strong>
          {data.description}
        </div>
        {data.attendees && Object.values(data.attendees).length > 0 && (
          <div className="trip-form">
            <img
              src={UserIcon}
              alt="User-icon"
              style={{
                verticalAlign: "middle",
                marginRight: "15px",
                width: "25px",
                height: "25px",
                marginTop: "15px",
              }}
            />
            <strong>Attendee Names(click username to email):</strong>
            {Object.values(data.attendees).map((attendee, index) => (
              <>
                <p>
                  <a
                    className="a-tag"
                    href={`mailto:${attendee.email}`}
                    style={{ cursor: "pointer" }}
                    title="contact me"
                  >
                    {attendee.username}
                  </a>
                </p>
              </>
            ))}
          </div>
        )}
      </div>
      <div>
        <br />
        <h1>Trip Location</h1>
        <Map center={center} zoom={zoom} markers={markers} />
        <br />
        <br />
      </div>
      <Footer />
    </div>
  );
}
